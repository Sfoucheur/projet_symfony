<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    private $repository;
    public function __construct(ProductRepository $productRepository){
        $this->repository=$productRepository;
    }
    /**
     * @Route("/product", name="product")
     */
    public function index()
    {
        $products = $this->repository->findAll();
        return $this->render('product/index.html.twig', [
            'controller_name' => 'productController',
            'products' => $products
            
        ]);
    }
    /**
     * @Route("/product/{id}", name="product.show")
     */
    public function show($id)
    {
        $product=$this->repository->find($id);
        if (!$product) {
            throw $this->createNotFoundException('The product does not exist');
        }
        return $this->render('product/show.html.twig', [
            'controller_name' => 'productController',
            'product' => $product,
        ]);
    }
    
}
