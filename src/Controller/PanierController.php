<?php

namespace App\Controller;

use App\Entity\Command;
use App\Form\CommandType;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

class PanierController extends AbstractController
{
    private $productrepository;
    public function __construct(ProductRepository $productRepository){
        $this->productrepository=$productRepository;
    }
    /**
     * @Route("/panier", name="panier")
     */
    public function index(SessionInterface $session)
    {
        $request = Request::createFromGlobals();
        $panier = $session->get('panier', []);
        $products = [];
        $total=0;
        foreach($panier as $id => $quantity) {
            $p =$this->productrepository->find($id);
            $products[$id] = [
                "product"=>$p,
                "quantite"=>$quantity
            ];
            $total+=$p->getPrice()*$quantity;
        }
        $command = new Command();
        $commandForm = $this->createForm(CommandType::class, $command);
        $commandForm->handleRequest($request);
        if ($commandForm->isSubmitted() && $commandForm->isValid()) {
            foreach($panier as $id => $quantity) {
                $command->addProduct($this->productrepository->find($id));
            }
            $command->setCreatedAt(new \DateTime);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($command);
            $manager->flush();
            $session->clear();
            $this->addFlash('success', "Les produits ont bien été commandé !");
            return $this->redirectToRoute('panier', [
                
            ]);
        }
        return $this->render('panier/index.html.twig', [
            'controller_name' => 'productController',
            'products' => $products,
            'total' => $total,
            'commandForm' => $commandForm->createView() 
            
        ]);
    }
    /**
     * @Route("/panier/delete/{id}", name="panier.delete")
     */
    public function delete($id,SessionInterface $session)
    {
        $request = Request::createFromGlobals();
        $csrfToken = $request->request->get('token');
        if ($this->isCsrfTokenValid('delete-item', $csrfToken)) {
            $product=$session->get('panier');
            if (!$product[$id]) {
                $this->addFlash('danger', "Le produit n'existe pas");
            }
            else{
                $panier = $session->get('panier', []);
                unset($panier[$id]);
                $session->set('panier', $panier);
                $panier = $this->productrepository->find($id);
                $this->addFlash('success', "Le produit {$panier->getName()} a bien été supprimé !");
            }
        }
        else{
            throw new InvalidCsrfTokenException;
        }
        return $this->redirectToRoute('panier', [
        ]);
    }
    /**
     * @Route("/panier/add/{id}", name="panier.add")
     */
    public function add($id,SessionInterface $session)
    {
        $product=$this->productrepository->find($id);
        if (!$product) {
            $this->addFlash('danger', "Le produit n'existe pas");
            return $this->json("nok",404);
        }
        else{
            $panier = $session->get('panier', []);
            $panier[$id] = 1;
            $session->set('panier', $panier);
            $this->addFlash('success', "Le produit {$product->getName()} a bien été ajouté !");
            return $this->json("ok",200);
        }
    }
    
}
