<?php

namespace App\Controller;

use App\Repository\CommandRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommandController extends AbstractController
{
    private $repository;
    public function __construct(CommandRepository $commandRepository){
        $this->repository=$commandRepository;
    }
    /**
     * @Route("/command", name="command")
     */
    public function index()
    {
        $commands = $this->repository->findAll();
        return $this->render('command/index.html.twig', [
            'controller_name' => 'CommandController',
            'commands' => $commands
        ]);
        
    }
    /**
     * @Route("/command{id}", name="command.show")
     */
    public function show($id)
    {
        $command=$this->repository->find($id);
        if (!$command) {
            throw $this->createNotFoundException('The command does not exist');
        }
        return $this->render('command/show.html.twig', [
            'controller_name' => 'CommandController',
            'command' => $command,
            'somme' => $command->getSum(),
            'nbproduit' => $command->getNbProducts()
        ]);
    }
}
